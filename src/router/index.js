import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/views/Home.vue'
import Calculator from '@/views/Calculator.vue'
import Data from '@/views/Data.vue'
import Imprint from '@/views/Imprint.vue'

const routes = [{
        path: '/',
        name: 'Home',
        component: Home
    },
    {
        path: '/calculator',
        name: 'Calculator',
        component: Calculator
    },
    {
        path: '/data',
        name: 'Data',
        component: Data
    },
    {
        path: '/imprint',
        name: 'Imprint',
        component: Imprint
    },
]

const router = createRouter({
    history: createWebHistory(),
    routes
})

export default router