export const AppModule = {
    namespaced: true,
    state: {
        navbar: {
            active_element: {
                calculator: false,
                data: false,
                imprint: false
            }
        },
        dialog: {
            visibility: false
        }
    },
    mutations: {
        activate_nav_element(state, element) {
            state.navbar.active_element[element] = true
        },
        deactivate_nav_element(state, element) {
            state.navbar.active_element[element] = false
        },
        show_dialog(state, ) {
            state.dialog.visibility = true
        },
        hide_dialog(state) {
            state.dialog.visibility = false
        },
    },
    actions: {
        activate_nav_element(context, element) {
            context.commit("activate_nav_element", element)
        },
        deactivate_nav_element(context, element) {
            context.commit("deactivate_nav_element", element)
        },
        show_dialog(context) {
            context.commit("show_dialog")
        },
        hide_dialog(context) {
            context.commit("hide_dialog")
        },
    },
    getters: {},
}