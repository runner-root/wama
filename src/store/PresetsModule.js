export const PresetsModule = {
    namespaced: true,
    state: {
        Factions: {},
        Datasheets: {},
        Wargear: {},
        WargearAbilities: new Set()
    },
    mutations: {
        set_preset_table(state, data) {
            state[data.table] = data.data
        },
    },
    actions: {
        load_database_export(context, database) {
            for (const table in database) {
                console.log(`importing ${table}`);
                context.commit("set_preset_table", { table: table, data: database[table] })
            }
        },
        parse_raw_data(context) {
            let loading_order = [
                "Factions",
                "Datasheets",
                "Datasheets_models",
                "Datasheets_damage",
                "Wargear",
                "Wargear_list",
                "Datasheets_wargear"
            ]

            for (const index in loading_order) {
                let raw_table_name = loading_order[index]
                let preset_table_name
                let parsing_error = false
                let raw_data = [...context.rootState.DataImport.wahapedia.raw_data[raw_table_name]]
                let parsed_table = {}

                // remove (last) empyt entry from raw data
                raw_data.pop()

                // remove headers from raw data
                let headers = raw_data.shift()

                // decide preset table to save parsed data to
                switch (raw_table_name) {
                    case "Factions":
                        preset_table_name = "Factions";
                        break;
                    case "Wargear":
                        preset_table_name = "Wargear";
                        break;
                    case "Wargear_list":
                        preset_table_name = "Wargear";
                        break;
                    default:
                        preset_table_name = "Datasheets";
                        break;
                }

                // losely verify table health
                switch (raw_table_name) {
                    case "Factions":
                        if (headers[0] != 'id' || headers[1] != 'name' || headers.length != 4) {
                            parsing_error = true
                        }
                        break;

                    case "Datasheets":
                        if (headers[0] != 'id' || headers[1] != 'name' || headers[3] != 'faction_id' || headers.length != 16) {
                            parsing_error = true
                        }
                        break;

                    case "Datasheets_models":
                        if (headers[0] != 'datasheet_id' ||
                            headers[2] != 'name' ||
                            headers[3] != 'M' ||
                            headers[4] != 'WS' ||
                            headers[5] != 'BS' ||
                            headers[6] != 'S' ||
                            headers[7] != 'T' ||
                            headers[8] != 'W' ||
                            headers[9] != 'A' ||
                            headers[10] != 'Ld' ||
                            headers[11] != 'Sv' ||
                            headers.length != 17) {
                            parsing_error = true
                        }
                        break;

                    case "Datasheets_damage":
                        if (headers[0] != 'datasheet_id' ||
                            headers.length != 8) {
                            parsing_error = true
                        }
                        break;

                    case "Wargear":
                        if (headers[0] != 'id' ||
                            headers[1] != 'name' ||
                            headers[2] != 'type' ||
                            headers[3] != 'description' ||
                            headers[4] != 'cost_description' ||
                            headers[5] != 'cost' ||
                            headers[6] != 'source_id' ||
                            headers[7] != 'is_relic' ||
                            headers.length != 9) {
                            parsing_error = true
                        }
                        break;

                    case "Wargear_list":
                        if (headers[0] != 'wargear_id' ||
                            headers[1] != 'line' ||
                            headers[2] != 'name' ||
                            headers[3] != 'Range' ||
                            headers[4] != 'type' ||
                            headers[5] != 'S' ||
                            headers[6] != 'AP' ||
                            headers[7] != 'D' ||
                            headers[8] != 'abilities' ||
                            headers.length != 10) {
                            parsing_error = true
                        }
                        break;

                    case "Datasheets_wargear":
                        if (headers[0] != 'datasheet_id' ||
                            headers[1] != 'line' ||
                            headers[2] != 'wargear_id' ||
                            headers[3] != 'cost' ||
                            headers[4] != 'is_index_wargear' ||
                            headers[5] != 'model' ||
                            headers[6] != 'is_upgrade' ||
                            headers.length != 8) {
                            parsing_error = true
                        }
                        break;

                    default:
                        parsing_error = true
                        break;
                }

                // handle parsing errors
                if (parsing_error) {
                    context.dispatch("DataImport/set_wahapedia_upload_state", {
                        table: raw_table_name,
                        upload_state: "error",
                    })
                    return
                }

                // build table
                switch (raw_table_name) {
                    case "Factions":
                        console.log('handling faction import');
                        parsed_table = {...context.state[preset_table_name] }
                        raw_data.forEach(element => {
                            parsed_table[element[0]] = {
                                name: element[1],
                                datasheets: {}
                            }
                        });
                        break;

                    case "Datasheets":
                        console.log('handling datasheet import');
                        parsed_table = {...context.state[preset_table_name] }
                        raw_data.forEach(element => {
                            parsed_table[element[0]] = {
                                unit_name: element[1],
                                faction_id: element[3],
                                battlefield_role: element[5],
                                models: {}
                            }
                        });
                        break;

                    case "Datasheets_models":
                        console.log('handling model import');
                        parsed_table = {...context.state[preset_table_name] }
                        raw_data.forEach(element => {
                            // check if datasheet exists
                            if (!(element[0] in parsed_table)) {
                                console.log(`no datasheet (${element[0]}) for model ${element[2]}`);
                                return
                            }

                            // typing 
                            for (const index in element) {
                                if (element[index] == '-') {
                                    element[index] = 0
                                }
                            }
                            let ws = Number(String(element[4]).substring(0, element[4].length - 1))
                            let bs = Number(String(element[5]).substring(0, element[5].length - 1))
                            let sv = Number(String(element[11]).substring(0, element[11].length - 1))

                            parsed_table[element[0]].models[element[2]] = {
                                wounds: element[8],
                                wargear: [],
                                brackets: {
                                    base: {
                                        movement: element[3],
                                        weapon_skill: ws,
                                        balistic_skill: bs,
                                        strength: element[6],
                                        toughtness: element[7],
                                        remaining_wounds: element[8],
                                        attacks: element[9],
                                        leadership: element[10],
                                        save: sv
                                    },
                                    1: {
                                        movement: element[3],
                                        weapon_skill: ws,
                                        balistic_skill: bs,
                                        strength: element[6],
                                        toughtness: element[7],
                                        remaining_wounds: element[8],
                                        attacks: element[9],
                                        leadership: element[10],
                                        save: sv
                                    }
                                }
                            }
                        });
                        break;

                    case "Datasheets_damage":
                        console.log('handling models damage table import');
                        parsed_table = {...context.state[preset_table_name] }
                        var bracket_header

                        // add damage table to models
                        raw_data.forEach(element => {
                            // check if datasheet exists
                            if (!(element[0] in parsed_table)) {
                                console.log(`no datasheet (${element[0]}) for bracket`);
                                return
                            }
                            // detect header if line is 0
                            if (element[1] == 0) {
                                bracket_header = element
                                return
                            }
                            // check if bracket applies to one or all models
                            if (bracket_header.includes("Model")) {
                                let model_name = element[bracket_header.indexOf("Model")]
                                handle_brackets_for_model(model_name)
                            } else {
                                for (const model_name in parsed_table[element[0]].models) {
                                    handle_brackets_for_model(model_name)
                                }
                            }

                            function handle_brackets_for_model(model_name) {
                                if (!(element[1] in parsed_table[element[0]].models[model_name].brackets)) {
                                    parsed_table[element[0]].models[model_name].brackets[element[1]] = {...parsed_table[element[0]].models[model_name].brackets.base }
                                }
                                // fill bracket with data
                                for (const index in element) {
                                    // do not handle first two columns
                                    if (index == 0 || index == 1) continue

                                    // match header to actual stat
                                    switch (bracket_header[index]) {
                                        case "RemainingW":
                                            parsed_table[element[0]].models[model_name].brackets[element[1]].remaining_wounds = element[index]
                                            break;

                                        case "M":
                                            parsed_table[element[0]].models[model_name].brackets[element[1]].movement = element[index]
                                            break;

                                        case "S":
                                            parsed_table[element[0]].models[model_name].brackets[element[1]].strength = element[index]
                                            break;

                                        case "A":
                                            parsed_table[element[0]].models[model_name].brackets[element[1]].attacks = element[index]
                                            break;

                                        case "BS":
                                            var bs = Number(String(element[index]).substring(0, element[index].length - 1))
                                            parsed_table[element[0]].models[model_name].brackets[element[1]].balistic_skill = bs
                                            break;

                                        case "WS":
                                            var ws = Number(String(element[index]).substring(0, element[index].length - 1))
                                            parsed_table[element[0]].models[model_name].brackets[element[1]].weapon_skill = ws
                                            break;

                                        default:
                                            if (bracket_header[index] && bracket_header[index] != "Model") {
                                                parsed_table[element[0]].models[model_name].brackets[element[1]].special = {
                                                    [bracket_header[index]]: element[index]
                                                }
                                            }
                                            break;
                                    }
                                }
                            }

                        });
                        break;

                    case "Wargear":
                        console.log('handling wargear import');
                        parsed_table = {...context.state[preset_table_name] }
                        raw_data.forEach(element => {
                            parsed_table[element[0]] = {
                                name: element[1],
                                type: element[2],
                                is_relic: element[7],
                                profiles: {}
                            }
                        });
                        break;

                    case "Wargear_list":
                        console.log('handling wargear profiles import');
                        parsed_table = {...context.state[preset_table_name] }

                        // add profiles to wargear
                        raw_data.forEach(element => {
                            // check if wargear exists
                            if (!(element[0] in parsed_table)) {
                                console.log(`'no wargear for ${element[0]}'`);
                                return
                            }

                            // check if profile exist, if not create empty
                            if (!(element[2] in parsed_table[element[0]].profiles)) {
                                parsed_table[element[0]].profiles[element[2]] = {}
                            }

                            // handle abilities
                            let abillities = String(element[8]).replace(/(<([^>]+)>)/ig, '')
                            context.state.WargearAbilities.add(abillities)

                            // handle melee weapons
                            if (element[4] == 'Melee') {
                                parsed_table[element[0]].profiles[element[2]] = {
                                    range: element[3],
                                    type: element[4],
                                    attacks: 0,
                                    strength: element[5],
                                    armor_penetration: element[6],
                                    damage: element[7],
                                    abilities: abillities,
                                }
                                return
                            }

                            // handle ranged weapons
                            if (String(element[4]).startsWith("Assault") ||
                                String(element[4]).startsWith("Grenade") ||
                                String(element[4]).startsWith("Heavy") ||
                                String(element[4]).startsWith("Pistol") ||
                                String(element[4]).startsWith("Rapid Fire")) {
                                let splitted = String(element[4]).split(" ")
                                let type
                                let attacks
                                switch (splitted.length) {
                                    case 2:
                                        type = splitted[0]
                                        attacks = splitted[1]
                                        break;
                                    case 3:
                                        type = `"${splitted[0]} ${splitted[1]}"`
                                        attacks = splitted[2]
                                        break;

                                    default:
                                        console.log(`"weird stuff happened with type ${element[4]} in ${element[0]}"`);
                                        break;
                                }
                                parsed_table[element[0]].profiles[element[2]] = {
                                    range: element[3],
                                    type: type,
                                    attacks: attacks,
                                    strength: element[5],
                                    armor_penetration: element[6],
                                    damage: element[7],
                                    abilities: abillities,
                                }
                                return
                            }

                            if (element[4]) {
                                console.log(`"unknown type ${element[4]} in ${element[0]}"`);
                            }
                        });
                        break;

                    case "Datasheets_wargear":
                        console.log('handling wargear for datasheets import');
                        parsed_table = {...context.state[preset_table_name] }

                        // add profiles to wargear
                        raw_data.forEach(element => {
                            // check if datasheet exists
                            if (!(element[0] in parsed_table)) {
                                console.log(`no datasheet (${element[0]}) for wargear ${element[2]}`);
                                return
                            }

                            // check if wargear exists
                            if (!(element[2] in context.state.Wargear)) {
                                console.log(`wargear ${element[2]} not found for datasheet ${element[0]}`);
                                return
                            }

                            // handle target model
                            let target_model = element[5]
                            if (target_model == null) {
                                target_model = 'all'
                            }

                            // check if model exists in datasheet
                            if (target_model != 'all' && !(target_model in parsed_table[element[0]].models)) {
                                console.log(`model ${target_model} not in datasheet (${element[0]}) for wargear ${element[2]}`);
                                return
                            }

                            // add wargear object to datasheet
                            if (target_model == 'all') {
                                for (const model_name in parsed_table[element[0]].models) {
                                    add_wargear_to_model(model_name)
                                }
                            } else {
                                add_wargear_to_model(target_model)
                            }

                            function add_wargear_to_model(model_name) {
                                if (!(parsed_table[element[0]].models[model_name].wargear.includes(element[2]))) {
                                    parsed_table[element[0]].models[model_name].wargear.push(element[2])
                                } else {
                                    console.log(`wargear ${element[2]} already exists for ${model_name} in datasheet ${element[0]}`);
                                }
                            }

                        });
                        break;

                    default:
                        console.log(`"No parsing rule detected for: ${raw_table_name}"`);
                        return
                }

                context.commit("set_preset_table", { table: preset_table_name, data: parsed_table })
            }

            // connect datasheets and factions
            let tmp_datasheets = {}
            for (const faction_id in context.state.Factions) {
                tmp_datasheets[faction_id] = context.state.Factions[faction_id]
            }
            for (const datasheet_id in context.state.Datasheets) {
                tmp_datasheets[context.state.Datasheets[datasheet_id].faction_id].datasheets[datasheet_id] = {...context.state.Datasheets[datasheet_id] }
            }
            context.commit("set_preset_table", { table: "Datasheets", data: tmp_datasheets })
            console.log(context.state.Datasheets);
            console.log(context.state.Wargear);
            console.log(context.state.WargearAbilities);
        }
    },
    getters: {
        get_preset_json: (state) => {
            let presets = {
                Datasheets: state.Datasheets,
                Wargear: state.Wargear,
                WargearAbilities: state.WargearAbilities
            }

            let presets_json = JSON.stringify(presets, null, 4)
            return presets_json
        },
        get_units_by_faction: (state) => (faction_id) => {
            if (faction_id == null) {
                return {}
            }
            return state.Datasheets[faction_id].datasheets
        },
        get_models_by_datasheet: (state) => (faction_id, datasheet_id) => {
            if (datasheet_id == null || faction_id == null) {
                return {}
            }
            return state.Datasheets[faction_id].datasheets[datasheet_id].models
        },
        get_wargear_by_model: (state) => (faction_id, datasheet_id, model_id) => {
            if (datasheet_id == null || faction_id == null || model_id == null) {
                return {}
            }

            let weapons = {}
            for (const index in state.Datasheets[faction_id].datasheets[datasheet_id].models[model_id].wargear) {
                let weapon_id = state.Datasheets[faction_id].datasheets[datasheet_id].models[model_id].wargear[index]
                weapons[weapon_id] = state.Wargear[weapon_id]
            }

            return weapons
        },
    },
}