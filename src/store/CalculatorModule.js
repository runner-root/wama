import lookup_table_json from "@/assets/data/lookup_table.json";

export const CalculatorModule = {
    namespaced: true,
    state: {
        lookup_table: lookup_table_json,
        attacker: {
            faction: null,
            datasheet: null,
            models: []
        },
        defender: {
            faction: null,
            datasheet: null,
            models: null
        }
    },
    mutations: {
        set_attacker_faction(state, faction_id) {
            state.attacker.faction = faction_id
        },
        set_attacker_datasheet(state, datasheet_id) {
            state.attacker.datasheet = datasheet_id
        },
        set_attacker_model(state, data) {
            // model / bracket / count / stats
            if (data.count == 0) {
                if (data.model_id in state.attacker.models) {
                    if (data.bracket in state.attacker.models[data.model_id]) {
                        delete state.attacker.models[data.model_id][data.bracket]
                    }
                    if (Object.keys(state.attacker.models[data.model_id]).length == 0) {
                        delete state.attacker.models[data.model_id]
                    }
                }
                return
            }

            if (!(data.model_id in state.attacker.models)) {
                state.attacker.models[data.model_id] = {}
            }

            state.attacker.models[data.model_id][data.bracket] = {}

            for (let number = 0; number < data.count; number++) {
                state.attacker.models[data.model_id][data.bracket][number] = {
                    movement: data.model_blueprint.brackets[data.bracket].movement,
                    weapon_skill: data.model_blueprint.brackets[data.bracket].weapon_skill,
                    balistic_skill: data.model_blueprint.brackets[data.bracket].balistic_skill,
                    strength: data.model_blueprint.brackets[data.bracket].strength,
                    toughtness: data.model_blueprint.brackets[data.bracket].toughtness,
                    remaining_wounds: data.model_blueprint.brackets[data.bracket].remaining_wounds,
                    attacks: data.model_blueprint.brackets[data.bracket].attacks,
                    leadership: data.model_blueprint.brackets[data.bracket].leadership,
                    save: data.model_blueprint.brackets[data.bracket].save,
                    weapons: {}
                }
                for (let index in data.model_blueprint.wargear) {
                    let wargear_id = data.model_blueprint.wargear[index];
                    state.attacker.models[data.model_id][data.bracket][number].weapons[wargear_id] = {
                        count: 0,
                        profiles: []
                    }
                }
            }
        },
        flush_attacker_models(state) {
            state.attacker.models = {}
        },
        set_attacker_weapon_count(state, data) {
            state.attacker.models[data.model_id][data.bracket][data.id].weapons[data.wargear_id].count = data.count
        },
        set_attacker_weapon_profile(state, data) {
            let profiles = state.attacker.models[data.model_id][data.bracket][data.id].weapons[data.wargear_id].profiles
            if (data.value) {
                if (!profiles.includes(data.profile)) {
                    profiles.push(data.profile)
                }
            } else {
                if (profiles.includes(data.profile)) {
                    const index = profiles.indexOf(data.profile)
                    profiles.splice(index, 1)
                }
            }
        },
        set_defender_faction(state, faction_id) {
            state.defender.faction = faction_id
        },
        set_defender_datasheet(state, datasheet_id) {
            state.defender.datasheet = datasheet_id
        },
        flush_defender_model(state) {
            state.defender.model = null
        },
        set_defender_model(state, model_id) {
            state.defender.model = model_id
        },
    },
    actions: {
        set_attacker_faction(context, faction_id) {
            context.commit("set_attacker_faction", faction_id)
        },
        set_attacker_datasheet(context, datasheet_id) {
            context.commit("set_attacker_datasheet", datasheet_id)
        },
        set_attacker_model(context, data) {
            context.commit("set_attacker_model", data)
        },
        flush_attacker_models(context) {
            context.commit("flush_attacker_models")
        },
        set_attacker_weapon_count(context, data) {
            context.commit("set_attacker_weapon_count", data)
        },
        set_attacker_weapon_profile(context, data) {
            context.commit("set_attacker_weapon_profile", data)
        },
        set_defender_faction(context, faction_id) {
            context.commit("set_defender_faction", faction_id)
        },
        set_defender_datasheet(context, datasheet_id) {
            context.commit("set_defender_datasheet", datasheet_id)
        },
        set_defender_model(context, model_id) {
            context.commit("set_defender_model", model_id)
        },
        flush_defender_model(context) {
            context.commit("flush_defender_model")
        },
    },
    getters: {
        // get_calculated_weapon_strength: (state) => (weapon) => {
        //     let weapon_strength = state.attacker.weapon_stats[weapon][
        //         "strength"
        //     ];
        //     let model_strength = Number(state.attacker.stats.strength)

        //     if (weapon_strength == "x2") {
        //         return model_strength * 2;
        //     }

        //     if (weapon_strength == "User") {
        //         return model_strength;
        //     }

        //     if (String(weapon_strength).includes("+")) {
        //         return model_strength + Number(weapon_strength.substring(1));
        //     }

        //     return Number(weapon_strength);
        // },
        // get_damage_sum: (state, getters) => (distance, weapons) => {
        //     let sum = 0
        //     for (const weapon in weapons[distance]) {
        //         let mortals = getters.get_mortal_wound_count(weapons[distance][weapon], distance);
        //         let damage = getters.get_inflicted_damage_count(weapons[distance][weapon], distance)
        //         sum = sum + mortals + damage
        //     }
        //     return sum
        // },
        // get_mortal_wound_count: (state, getters) => (weapon, distance) => {
        //     let wounds = getters.get_wound_count(weapon, distance);
        //     return 0 * wounds
        // },
        // get_inflicted_damage_count: (state, getters) => (weapon, distance) => {
        //     let possible_damage = getters.get_possible_damage_count(weapon, distance);
        //     let FNP_chance = (7 - Number(state.defender.stats.FNP)) / 6

        //     return possible_damage * (1 - FNP_chance)
        // },
        // get_possible_damage_count: (state, getters) => (weapon, distance) => {
        //     let unsaved = getters.get_unsaved_count(weapon, distance);
        //     let damage

        //     if (String(state.attacker.weapon_stats[weapon].damage).includes("D")) {
        //         damage = state.lookup_table['dice'][state.attacker.weapon_stats[weapon].damage]
        //     } else {
        //         damage = Number(state.attacker.weapon_stats[weapon].damage);
        //     }

        //     return unsaved * damage
        // },
        // get_unsaved_count: (state, getters) => (weapon, distance) => {
        //     let wounds = getters.get_wound_count(weapon, distance);
        //     let ap = Number(state.attacker.weapon_stats[weapon].armor_penetration);
        //     let saving_roll;
        //     let save = Number(state.defender.stats.save)
        //     let invuln = Number(state.defender.stats.invuln)
        //     if (invuln < (save - ap)) {
        //         saving_roll = invuln;
        //     } else {
        //         saving_roll = save - ap;
        //     }

        //     let save_chance = (7 - saving_roll) / 6;
        //     return wounds * (1 - save_chance);
        // },
        // get_wound_count: (state, getters) => (weapon, distance) => {
        //     let hits = getters.get_hit_count(weapon, distance)
        //     let attack_strength = getters.get_calculated_weapon_strength(weapon)
        //     let defend_toughness = Number(state.defender.stats.toughness)
        //     let wound_chance = 0
        //     if (attack_strength >= 2 * defend_toughness) {
        //         wound_chance = 5 / 6
        //     } else if (attack_strength > defend_toughness) {
        //         wound_chance = 4 / 6
        //     } else if (attack_strength == defend_toughness) {
        //         wound_chance = 3 / 6
        //     } else if (2 * attack_strength <= defend_toughness) {
        //         wound_chance = 1 / 6
        //     } else {
        //         wound_chance = 2 / 6
        //     }

        //     return hits * wound_chance
        // },
        // get_hit_count: (state, getters) => (weapon, distance) => {
        //     let attacks = getters.get_attack_count(weapon, distance)
        //     let hit_skill
        //     if (state.attacker.weapon_stats[weapon].type == "Melee") {
        //         hit_skill = Number(state.attacker.stats.weapon_skill)
        //     } else {
        //         hit_skill = Number(state.attacker.stats.balistic_skill)
        //     }
        //     let hit_chance = (7 - hit_skill) / 6
        //     return attacks * hit_chance
        // },
        // get_attack_count: (state) => (weapon, distance) => {
        //     let count = Number(state.attacker.weapon_stats[weapon].count);
        //     let attacks;
        //     let range_multiplier = 1;
        //     if (state.attacker.weapon_stats[weapon].type == "Melee") {
        //         count = 1;
        //         attacks = Number(
        //             state.attacker.weapon_stats[weapon].attacks
        //         );
        //     } else {
        //         if (String(state.attacker.weapon_stats[weapon].shots).includes("D")) {
        //             attacks = state.lookup_table['dice'][state.attacker.weapon_stats[weapon].shots]
        //         } else {
        //             attacks = Number(state.attacker.weapon_stats[weapon].shots);
        //         }

        //     }
        //     if (
        //         state.attacker.weapon_stats[weapon].type == "Rapid Fire" &&
        //         distance <= state.attacker.weapon_stats[weapon].range / 2
        //     ) {
        //         range_multiplier = 2;
        //     }
        //     return count * attacks * range_multiplier;
        // }
    },
}