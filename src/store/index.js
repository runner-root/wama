import { createStore } from 'vuex'

import { AppModule } from "./AppModule"
import { PresetsModule } from "./PresetsModule"
import { DataImportModule } from "./DataImportModule"
import { CalculatorModule } from "./CalculatorModule"


export default createStore({
    state: {},
    mutations: {},
    actions: {},
    getters: {},
    modules: {
        App: AppModule,
        Presets: PresetsModule,
        DataImport: DataImportModule,
        Calculator: CalculatorModule
    },
})