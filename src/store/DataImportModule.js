export const DataImportModule = {
    namespaced: true,
    state: {
        wahapedia: {
            source: {
                Factions: "https://wahapedia.ru/wh40k9ed/Factions.csv",
                // Source: "https://wahapedia.ru/wh40k9ed/Source.csv",
                Datasheets: "https://wahapedia.ru/wh40k9ed/Datasheets.csv",
                // Datasheets_abilities: "https://wahapedia.ru/wh40k9ed/Datasheets_abilities.csv",
                Datasheets_damage: "https://wahapedia.ru/wh40k9ed/Datasheets_damage.csv",
                // Datasheets_keywords: "https://wahapedia.ru/wh40k9ed/Datasheets_keywords.csv",
                Datasheets_models: "https://wahapedia.ru/wh40k9ed/Datasheets_models.csv",
                // Datasheets_options: "https://wahapedia.ru/wh40k9ed/Datasheets_options.csv",
                Datasheets_wargear: "https://wahapedia.ru/wh40k9ed/Datasheets_wargear.csv",
                Wargear: "https://wahapedia.ru/wh40k9ed/Wargear.csv",
                Wargear_list: "https://wahapedia.ru/wh40k9ed/Wargear_list.csv",
                // Stratagems: "https://wahapedia.ru/wh40k9ed/Stratagems.csv",
                // Abilities: "https://wahapedia.ru/wh40k9ed/Abilities.csv",
            },
            uploaded: {
                Factions: false,
                // Source: false,
                Datasheets: false,
                // Datasheets_abilities: false,
                Datasheets_damage: false,
                // Datasheets_keywords: false,
                Datasheets_models: false,
                // Datasheets_options: false,
                Datasheets_wargear: false,
                Wargear: false,
                Wargear_list: false,
                // Stratagems: false,
                // Abilities: false,
            },
            all_upload_complete: false,
            raw_data: {}
        },
    },
    mutations: {
        set_wahapedia_upload_state(state, data) {
            state.wahapedia.uploaded[data.table] = data.upload_state
        },
        set_all_wahapedia_upload_state(state, data) {
            state.wahapedia.all_upload_complete = data
        },
        set_wahapedia_raw_data(state, data) {
            state.wahapedia.raw_data[data.table] = data.data
        },
    },
    actions: {
        set_wahapedia_upload_state(context, data) {
            context.commit("set_wahapedia_upload_state", data)
            for (const key in context.state.wahapedia.uploaded) {
                if (!context.state.wahapedia.uploaded[key]) {
                    context.commit("set_all_wahapedia_upload_state", false)
                    return
                }
            }
            context.commit("set_all_wahapedia_upload_state", true)
        },
        set_wahapedia_raw_data(context, data) {
            context.commit("set_wahapedia_raw_data", data)
        },
    },
    getters: {},
}