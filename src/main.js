import { createApp } from 'vue';
import wama from './App.vue';
import store from './store';
import router from './router';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faCheckCircle, faExclamationTriangle, faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add({ faCheckCircle, faExclamationTriangle, faTimesCircle })

const app = createApp(wama);

app.use(router);
app.use(store);
app.component('font-awesome-icon', FontAwesomeIcon)
app.config.productionTip = false

app.mount('#app');