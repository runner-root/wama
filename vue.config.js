/**
 * @type {import('@vue/cli-service').ProjectOptions}
 */
module.exports = {
    css: {
        loaderOptions: {
            sass: {
                additionalData: "@import '@/assets/css/base.scss';"
            }
        }
    },
    publicPath: process.env.NODE_ENV === 'production' ?
        '/' : '/'
}